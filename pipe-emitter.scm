#! /usr/bin/guile -s
!#
(use-modules (ice-9 binary-ports)
             (rnrs bytevectors)
             (ice-9 iconv))
(let ((output (string->utf8 "hello world\n")))
  (put-bytevector (current-output-port) output)
  (close (current-output-port))) ;; testing some handling


