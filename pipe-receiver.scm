#! /usr/bin/guile -s
!#
(use-modules (ice-9 binary-ports)
             (rnrs bytevectors))
(let ((input (get-bytevector-all (current-input-port))))
  (cond
   ((eof-object? input)
    (begin
      (display input)
      (newline)))
   (#t (begin
         (display (bytevector-length input))
         (newline)
         (put-bytevector (current-output-port) input)))))
